Insert into PRODUCT (id,name,description) values(1,'Tenis','Nike brand')
Insert into PRODUCT (id,name,description) values(2,'T-Shirt','Nike brand')
Insert into PRODUCT (id,name,description) values(3,'Short','Nike brand')
Insert into PRODUCT (id,name,description) values(4,'Cap','Nike brand')
Insert into PRODUCT (id,name,description) values(5,'Belt','Nike brand')
Insert into PRODUCT (id,name,description) values(6,'Skirt','Nike brand')
Insert into PRODUCT (id,name,description) values(7,'Underwear','Nike brand')
Insert into PRODUCT (id,name,description) values(8,'Socks','Nike brand')
Insert into PRODUCT (id,name,description) values(9,'Watch','Nike brand')
Insert into PRODUCT (id,name,description) values(10,'Phone','Nike brand')




INSERT INTO IMAGE (id,type,product_id) values(1,'png',1)
INSERT INTO IMAGE (id,type,product_id) values(2,'png',1)
INSERT INTO IMAGE (id,type,product_id) values(3,'png',2)
INSERT INTO IMAGE (id,type,product_id) values(4,'png',3)
INSERT INTO IMAGE (id,type,product_id) values(5,'png',3)
INSERT INTO IMAGE (id,type,product_id) values(6,'png',5)
INSERT INTO IMAGE (id,type,product_id) values(7,'png',5)
INSERT INTO IMAGE (id,type,product_id) values(8,'png',4)
INSERT INTO IMAGE (id,type,product_id) values(9,'png',4)
INSERT INTO IMAGE (id,type,product_id) values(10,'png',4)


UPDATE PRODUCT SET parent_product_id = 4 where id = 1
UPDATE PRODUCT SET parent_product_id = 4 where id = 2
UPDATE PRODUCT SET parent_product_id = 1 where id = 3
UPDATE PRODUCT SET parent_product_id = 1 where id = 4
UPDATE PRODUCT SET parent_product_id = 2 where id = 5
UPDATE PRODUCT SET parent_product_id = 2 where id = 6
UPDATE PRODUCT SET parent_product_id = 3 where id = 7
UPDATE PRODUCT SET parent_product_id = 3 where id = 8
UPDATE PRODUCT SET parent_product_id = 5 where id = 9
UPDATE PRODUCT SET parent_product_id = 6 where id = 10 
