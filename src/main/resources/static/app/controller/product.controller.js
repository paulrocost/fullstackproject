angular.module('app').controller('productcontroller',['$scope', '$http','productservice','imageservice',
	function($scope,$http, productservice,imageservice){
	
	$scope.collection = [];
	$scope.imagecollection = [];
	$scope.showlistall = false;
	$scope.showImage = false;
	$scope.showlistChild = false;
	$scope.product = '';
	$scope.idproduct = null;
	$scope.description = null
	
	$scope.listall = function(){
		productservice.retrieveproductcollection()
			.then(function(response){
				if(response.data){
					$scope.collection = response.data;
					$scope.showlistall = true;
					$scope.showImage = false;
					$scope.showlistChild = false;
				}
			});
	}
	
	
	$scope.listallrelated = function(){
		productservice.retrieveproductandrelatedcollection()
			.then(function(response){
				if(response)
					$scope.collection = response;
			})
	}
	
	$scope.listproductbyid = function(id){
		productservice.retrieveproductsbyid(id)
			.then(function(response){
				if(response){
					$scope.product = response.data.name;
					$scope.idproduct = response.data.id;
					$scope.description = response.data.description;
				}
			})
	}
	
	$scope.listproductrelatedbyid = function(productid){
		productservice.retrieveproductrelatedbyid(productid)
			.then(function(response){
				if(response)
					$scope.collection = response.data;
			})
	}
	
	$scope.childsbyproductid = function(id){
		productservice.retrievechildsbyid(id)
			.then(function(response){
				if(response){
					$scope.collection = response.data;
					$scope.showlistChild = true;
					$scope.showlistall = false;
					$scope.showImage = false
				}
			})
	}
	
	$scope.imagesbyproductid = function(id){
		imageservice.retrieveimagesbyproductid(id)
			.then(function(response){
				if(response)
					$scope.imagecollection = response.data;
				$scope.showImage = true;
				$scope.showlistChild = false;
				$scope.showlistall = false;
			})
	}
	
}]);