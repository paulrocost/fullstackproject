angular.module('app').service('imageservice',['$http', function($http){
	var endpointapi = function(directory, parameter){
		var response = 'http://localhost:8080/image/' + directory;
		
		if(parameter){
			response += '/' + parameter;
		}
		
		return response;
	}
	
	this.retrieveimagesbyproductid = function(id){
		var endpoint = endpointapi('GetByProductId',id);
		return $http.post(endpoint);
	}
		
	
}]);