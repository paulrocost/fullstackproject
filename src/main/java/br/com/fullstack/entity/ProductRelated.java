package br.com.fullstack.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductRelated {

	private int id;
	private String name;
	private String description;
	private Collection<Product> products = new ArrayList<Product>();
	private Collection<Image> images = new ArrayList<Image>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Collection<Product> getProducts() {
		return products;
	}
	public void setProducts(Collection<Product> products) {
		this.products = products;
	}
	public Collection<Image> getImages() {
		return images;
	}
	public void setImages(Collection<Image> images) {
		this.images = images;
	}
	
	
	
}
