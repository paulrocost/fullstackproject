package br.com.fullstack.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fullstack.entity.Image;
import br.com.fullstack.entity.Product;
import br.com.fullstack.entity.ProductRelated;
import br.com.fullstack.repository.ProductRepository;

@Service
@Transactional
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	
	public @ResponseBody List<Product> getAll(){
		return productRepository.getAll();
	}
	
	public @ResponseBody List<Product> getProductRelatedAll(){	
		return productRepository.getProductRelatedAll();				 
	}
	
	public @ResponseBody Product getById(int id){		
		return productRepository.getById(id);
	}
	
	public List<Product> getProductRelatedAllById(Long id){		
		return productRepository.getProductRelatedAllById(id);
	}
	
	public @ResponseBody List<Product> getChilds(int id){		
		return productRepository.getChilds(id);
	}
}
