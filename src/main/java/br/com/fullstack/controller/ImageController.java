package br.com.fullstack.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fullstack.entity.Image;
import br.com.fullstack.service.ImageService;

@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RestController
@RequestMapping("/image")
public class ImageController {
	
	@Autowired
	ImageService imageService;
	
	@RequestMapping(method=RequestMethod.POST, value="/GetByProductId/{id}")
	public @ResponseBody List<Image> GetByProductId(@PathVariable(value="id")int id){
		return imageService.GetAllByProductId(id);
	}

}
