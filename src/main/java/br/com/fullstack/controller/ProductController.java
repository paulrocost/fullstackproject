package br.com.fullstack.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fullstack.entity.Product;
import br.com.fullstack.entity.ProductRelated;
import br.com.fullstack.service.ProductService;

@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@RequestMapping(method=RequestMethod.GET, value="/GetAll")
	public @ResponseBody List<Product> GetAll(){
		return productService.getAll();		
	}
	
	//not working yet
	@RequestMapping(method=RequestMethod.GET, value="/GetAllRelated")
	public @ResponseBody List<Product> GetAllRelated(){
		return productService.getProductRelatedAll();
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/GetById/{id}")
	public @ResponseBody Product GetById(@PathVariable(value="id")int id){
		return productService.getById(id);
		
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/ChildsByProductId/{id}")
	public @ResponseBody List<Product> ChildsByProductId(@PathVariable(value="id")int id){
		return productService.getChilds(id);
		
	}
	
	
	
	
}
