package br.com.fullstack.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import br.com.fullstack.entity.Image;

@Repository
public class ImageRepository {

	@PersistenceContext
	EntityManager entityManager;
	
	public List<Image> GetAllByProductId(int id){
		List<Image> list = (List<Image>)entityManager.createQuery("SELECT i FROM Image i Where i.product_id = :id").setParameter("id", id).getResultList();
		return list;
	}
}
