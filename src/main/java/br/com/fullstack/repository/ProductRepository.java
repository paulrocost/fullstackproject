package br.com.fullstack.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fullstack.entity.Product;


@Repository
public class ProductRepository{
	
	@PersistenceContext
	EntityManager entityManager;
	
	public @ResponseBody List<Product> getAll(){
		List<Product> list = (List<Product>)entityManager.createNativeQuery("SELECT * FROM Product ").getResultList();
		return list;
	}
	
	public @ResponseBody List<Product> getProductRelatedAll(){
		List<Product> list = (List<Product>)entityManager.createQuery("SELECT p FROM Product p").getResultList();
		return list;
	}
	
	public @ResponseBody Product getById(int id){
		Query q1 = entityManager.createNativeQuery("SELECT id,name,description,parent_product_id FROM Product  WHERE id = " + id, Product.class);
		Product product = (Product)q1.getSingleResult();
		return product;
	}
	
	public List<Product> getProductRelatedAllById(Long id){
		List<Product> list = (List<Product>)entityManager.createQuery("SELECT p FROM Product p left join Product pro on p.id = pro.product_parent_id left join Image i on i.product_id = p.id WHERE p.id = :id").setParameter("id", id).getResultList();
		return list;
	}
	
	public @ResponseBody List<Product> getChilds(int id){
		List<Product> list =  (List<Product>)entityManager.createNativeQuery("SELECT * FROM Product Where parent_product_id = " + id).getResultList();
		return list;
	}

}
