- Build the project and run using mvn spring-boot:run command
it will set up a h2 database and populate it with some data.
- if you access http://localhost:8080, there are some buttons at the screen that call RestApis for test.
- if you access http://localhost:8080/console you can check database. password 'sa'.

- About suite automated test unfortunately i have no skills with it, but i wish start a new project using this tests.
as this project has angularjs, i think all tests are done by interface.

- I had a problems with hibernate map, regarding self reference on Product table, hence i let two calls incomplete
numbers 2 and 4. when it returns it loads everything in a infinite loop. i've tried some stuff but no success.
I'll need to spend more time studying this problem later.


- I want to congratulate you guys by the project, i think it covered up several points on Java Web Project nowadays.
- Hope you guys enjoy my work.

Thanks.